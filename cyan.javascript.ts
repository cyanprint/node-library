import {Chalk} from "chalk";
import {Inquirer} from "inquirer";
import {Cyan, Documentation, DocUsage, Glob, IAutoInquire, IAutoMapper, IExecute} from "./Typings";

export = async function (folderName: string, chalk: Chalk, inquirer: Inquirer, autoInquirer: IAutoInquire, autoMap: IAutoMapper, execute: IExecute): Promise<Cyan> {
	
	let ide: any= {
		vs2017: "VS 2017",
		webStorm: "WebStorm",
		editor: "Text Editor (VS Code, Sublime, Atom etc)"
	};
	
	let ideAnswer: object = await autoInquirer.InquireAsList(ide, "Which IDE do you use?");
	let ideChoice: string = autoMap.ReverseLoopUp(ide, ideAnswer);
	
	let globs: Glob[] = [{root: "./Template/Common/", pattern: "**/*.*", ignore: []}];
	let guid: string[] = [];
	let NPM: string | boolean = true;
	
	let ideIgnore: { vs2017: string[], webStorm: string[] } = {
		vs2017: ["**/.idea/**/*"],
		webStorm: ["**/obj/**/*", "**/*.njsproj", "**/*.njsproj.user"]
	};
	
	//Pick files base on IDE
	if (ideChoice === ide.webStorm) {
		globs.push({root: "./Template/Javascript/var~cyan.folder.name~", pattern: "**/*.*", ignore: ideIgnore.webStorm})
	} else if (ideChoice === ide.vs2017) {
		NPM = folderName;
		globs.push({root: "./Template/Javascript/", pattern: "**/*.*", ignore: ideIgnore.vs2017});
		guid.push("DC5BB7F6-F769-4A0C-BF09-C8AF093FF897");
		guid.push("AE8FCFF8-C5EC-4664-9C33-722BACB087D7");
	} else {
		globs.push({
			root: "./Template/Javascript/var~cyan.folder.name~",
			pattern: "**/*.*",
			ignore: ideIgnore.vs2017.concat(ideIgnore.webStorm)
		})
	}
	//Ask for documentation
	let docQuestions: DocUsage = {license: false, git: false, readme: true, semVer: false, contributing: false};
	let docs: Documentation = await autoInquirer.InquireDocument(docQuestions);
	
	//Get read question to ask
	let variables: any = {
		name: {
			package: ["node-console-app", "Enter the name of your package"]
		}
	};
	
	//Get answers
	variables = await autoInquirer.InquireInput(variables);
	
	//Fill in empty information if document already have them
	variables.name.author = docs.data.author;
	variables.email = docs.data.email;
	variables.description = docs.data.description;
	
	let flags: any = {
		unitTest: false,
		packages: {
			//babel
			"@babel/core": false,
			"@babel/plugin-proposal-object-rest-spread": false,
			"@babel/preset-env": false,
			"babel-loader": false,
			//tools
			lodash: false,
			"@kirinnee/objex": false,
			//e2e
			testcafe: false,
			"html-webpack-plugin": false,
			//test
			chai: false,
			mocha: false,
			jest: false,
			nyc: false,
			//polyfill
			"@babel/polyfill": false,
			"whatwg-fetch": false,
			"dom4": false,
			//sass
			"node-sass": false,
			"sass-loader": false,
			//style
			"style-loader": false,
			"css-loader": false,
			"ignore-styles": false,
			"identity-obj-proxy": false,
			//frontend
			"jsdom": false,
			"jsdom-global": false,
			//minify
			"terser-webpack-plugin": false,
			"uglifyjs-webpack-plugin": true,
			//postcss
			"postcss-loader": false,
			"postcss-preset-env": false
		},
		move: {
			"@kirinnee/core": false
		},
		polyfill: false,
		cover: false,
		build: {
			distPolyfill: false,
			babel: false,
			style: false,
			sass: false,
			postcss: false,
			sourceMap: true,
			es6: false,
			console: false,
			unsafe: false,
			stripComment: false,
			renameGlobal: false
		},
		target: {
			web: false,
			node: false,
		},
		e2e: {
			chrome: {
				headless: false,
				real: false
			},
			firefox: {
				headless: false,
				real: false
			},
			"chrome-canary": {
				headless: false,
				real: false
			},
			ie: false,
			opera: false,
			edge: false
		}
	};
	
	if(ideChoice === ide.vs2017){
		flags.browserlist = true;
	}
	
	
	let target: any = {
		target: {
			web: "Front End",
			node: "Node"
		}
	};
	target = await autoInquirer.InquireAsList(target, "Which does this library target?");
	flags = autoMap.Overwrite(target, flags);
	
	target = target.target;
	if(target.web) {
		flags.packages.jsdom = true;
		flags.packages["jsdom-global"] = true;
	}
	//Ask for for features
	let features: any = {
		unitTest: "Unit Test",
		packages: {
			lodash: "Lodash",
			"@kirinnee/objex": "Objex",
		},
		move: {
			"@kirinnee/core": "Kirinnee Core Library"
		}
		
	};
	if (target.web) {
		features.packages.testcafe = "E2E (TestCafe Only)";
		features.build = {style: "StyleSheet"};
	}
	
	
	features = await autoInquirer.InquireAsCheckBox(features, "Which of these tools do you need?");
	flags = autoMap.Overwrite(features, flags);
	
	
	//if unit test
	if (flags.unitTest) {
		let coverage: boolean = await autoInquirer.InquirePredicate("Do you want to use coverage tool?");
		let mochaChai: any = {
			packages: {
				chai: true,
				mocha: true,
				nyc: coverage,
				rimraf: coverage,
			},
			cover: coverage
		};
		let jest: any = {
			packages: {
				jest: true
			},
			cover: coverage
		};
		let test: boolean = await autoInquirer.InquirePredicate("Which Test Framework do you want to use?", "Mocha + Chai", "Jest");
		flags = autoMap.Overwrite(test ? mochaChai : jest, flags);
		
	}
	
	delete flags.unitTest;
	
	//if e2e
	if (flags.packages.testcafe) {
		//Ask for polyfill
		let polyfill: any = {
			packages: {
				"@babel/polyfill": "Babel Polyfill",
				"whatwg-fetch": "Fetch API Polyfill",
				"dom4": "DOM4 Polyfill"
			}
		};
		
		polyfill = await autoInquirer.InquireAsCheckBox(polyfill, "Which do you this poly-fill do you need for testing?");
		polyfill.polyfill = polyfill.packages["@babel/polyfill"] || polyfill.packages["whatwg-fetch"] || polyfill.packages["dom4"];
		polyfill.packages["html-webpack-plugin"] = true;
		
		//Ask for browser testing
		let headless: any = {
			e2e: {
				chrome: {
					headless: "Google Chrome"
				},
				firefox: {
					headless: "Firefox"
				},
				"chrome-canary": {
					headless: "Google Chrome Canary"
				}
			}
		};
		headless = await autoInquirer.InquireAsCheckBox(headless, "Which of these headless browser do you want to use for E2E Tests?");
		
		let full: any = {
			e2e: {
				chrome: {
					real: "Google Chrome"
				},
				"chrome-canary": {
					real: "Google Chrome Canary"
				},
				firefox: {
					real: "Firefox"
				},
				ie: "Internet Explorer",
				opera: "Opera",
				
				edge: "Microsoft Edge"
			}
		};
		full = await autoInquirer.InquireAsCheckBox(full, "Which of these real browsers do you want to use for full E2E Tests?");
		let total: any = autoMap.JoinObjects(polyfill, full, headless);
		flags = autoMap.Overwrite(total, flags);
	}
	
	//Ask for build pipeline
	let builds: any = {
		build: {
			babel: "Babel 7",
			sourceMap: "Source Map",
			es6: "Minify ES6 (Use Terser instead of Uglify)",
			console: "Keep console.log",
			unsafe: "Unsafe Compression",
			stripComment: "Strip Comments",
			renameGlobal: "Top Level Mangle"
		}
	};
	if (target.web) {
		if (flags.build.polyfill) builds.build.distPolyfill = "Distribute with Polyfills (dist folder, not index)";
		if (features.build.style) {
			builds.build.sass = "Use SASS (require stylesheets)";
			builds.build.postcss = "Use PostCSS (require stylesheets)";
		}
	}
	
	builds = await autoInquirer.InquireAsCheckBox(builds, "Which of this features do you want in your build pipeline?");
	flags = autoMap.Overwrite(builds, flags);
	
	//Consolidate flags
	if (flags.build.sass) {
		flags.packages["sass-loader"] = true;
		flags.packages["node-sass"] = true;
	}
	
	if (flags.build.style) {
		flags.packages["style-loader"] = true;
		flags.packages["css-loader"] = true;
		if (flags.packages.chai) {
			flags.packages["ignore-styles"] = true
		}
		if (flags.packages.jest) {
			flags.packages["identity-obj-proxy"] = true;
		}
	}
	
	if (flags.build.es6) {
		flags.packages["uglifyjs-webpack-plugin"] = false;
		flags.packages["terser-webpack-plugin"] = true;
	}
	
	let browserlist: boolean = false;
	
	if (flags.build.postcss) {
		flags.packages["postcss-loader"] = true;
		flags.packages["postcss-preset-env"] = true;
		browserlist = true;
	} else {
		globs = globs.map(g => {
			g.ignore = (g.ignore as string[]).concat(["**/postcss.config.js"]);
			return g;
		});
	}
	
	if (flags.build.babel) {
		flags.packages["@babel/core"] = true;
		flags.packages["babel-loader"] = true;
		flags.packages["@babel/preset-env"] = true;
		flags.packages["@babel/plugin-proposal-object-rest-spread"] = true;
		browserlist = true;
	}
	
	if (!browserlist) {
		globs = globs.map(g => {
			g.ignore = (g.ignore as string[]).concat(["**/.browserslistrc"]);
			return g;
		});
		if(ideChoice === ide.vs2017){
			flags.browserlist = false;
		}
	}
	
	if (docs.usage.git) variables.git = docs.data.gitURL;
	
	return {
		globs: globs,
		flags: flags,
		variable: variables,
		docs: docs,
		guid: guid,
		comments: ["//"],
		npm : NPM
		
	} as Cyan;
	
}