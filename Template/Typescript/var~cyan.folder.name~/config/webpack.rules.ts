import {RuleSetRule, RuleSetUseItem} from "webpack";
import * as path from "path"; //flag~build.babel~

//if~build.style~
/*===================
 STYLE LOADER
 ===================== */
let preCSSLoaders: RuleSetUseItem[] = [
	{loader: 'postcss-loader', options: {config: {path: `${__dirname}/postcss.config.js`}}}, //flag~build.postcss~
	"sass-loader" //flag~build.sass~
];

let use: RuleSetUseItem[] = [
	"style-loader",
	{loader: "css-loader", options: {importLoaders: preCSSLoaders.length}}
];

let cssRule: RuleSetRule = {
	test: /\.s?css/,
	use: use.concat(preCSSLoaders)
};
//end~build.style~


/*===================
  TS LOADER
 ===================== */

let uses: RuleSetUseItem[] = [
	{loader: 'babel-loader', options: {configFile: path.resolve(__dirname, "./flag~build.babel~babel.config.js")}}, //flag~build.babel~
	{loader: 'ts-loader'}
];

let scripts: RuleSetRule = {
	test: /\.tsx?$/,
	exclude: /(node_modules|bower_components)/,
	use: uses
};

let rules: RuleSetRule[] = [
	scripts,
	cssRule //flag~build.style~
];

export {rules};
