import {Shape, Square} from "../../src";

// @ts-ignore
test("Square", () => {
	let rect: Shape = new Square(7);
	
	expect(rect.area).toBe(49);
	expect(rect.parameter()).toBe(28);
});