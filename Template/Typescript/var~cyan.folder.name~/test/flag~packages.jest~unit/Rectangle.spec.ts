import {Rectangle, Shape} from "../../src";

// @ts-ignore
test("Rectangle", () => {
	
	let rect: Shape = new Rectangle(5, 10);
	
	expect(rect.area).toBe(50);
	expect(rect.parameter()).toBe(30);
	
});