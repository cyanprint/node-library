import {ShapeDataViewer, Shape} from "../../src";
import {stubInterface} from "ts-sinon";

// @ts-ignore
test("ShapeDataViewer", () => {
	
	let dataview: ShapeDataViewer = new ShapeDataViewer();
	
	let circle: Shape = stubInterface<Shape>();
	circle.area = 40;
	(circle.parameter as any).returns(50);
	
	expect(dataview.GetData(circle)).toBe("area: 40 parameter: 50");
	
});