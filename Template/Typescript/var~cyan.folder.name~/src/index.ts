import {Core, Kore} from "@kirinnee/core"; //flag~move.@kirinnee/core~
import {Shape} from "./classLibrary/Shape";
import {Rectangle} from "./classLibrary/Rectangle";
import {Square} from "./classLibrary/Square";
import {ShapeDataViewer} from "./classLibrary/flag~mock~ShapeDataViewer";
import "./flag~build.sass~index.scss";
import "./flag~build.style~index.css";
import  {ObjectX, Objex} from "@kirinnee/objex"; //flag~packages.@kirinnee/objex~
import  _ from 'lodash'; //flag~packages.lodash~

let core:Core = new Kore();//flag~move.@kirinnee/core~
core.ExtendPrimitives();//flag~move.@kirinnee/core~

let objex:Objex = new ObjectX(core); //flag~packages.@kirinnee/objex~
objex.ExtendPrimitives(); //flag~packages.@kirinnee/objex~

//if~target.web~
if(document.querySelectorAll("button")[0]!=null){
	document.querySelectorAll("button")[0].onclick = function(){
		document.getElementById("target")!.innerText = (document.getElementById("input") as HTMLInputElement).value.split('').reverse().join('');
	};
}

//end~target.web~


export {
	ShapeDataViewer,//flag~mock~
	Shape, Rectangle, Square
}
