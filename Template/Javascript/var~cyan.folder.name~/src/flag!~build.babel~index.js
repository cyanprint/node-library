var Square = require("./Square");
var Rectangle = require("./Rectangle");
var _ = require("lodash"); //flag~packages.lodash~
var Kore = require("@kirinnee/core").Kore; //flag~move.@kirinnee/core~
var ObjectX = require("@kirinnee/objex").ObjectX; //flag~packages.@kirinnee/objex~
require('./flag~build.sass~index.scss');
require("./flag~build.style~index.css");

//if~move.@kirinnee/core~
var core = new Kore();
core.ExtendPrimitives();
//end~move.@kirinnee/core~
//if~packages.@kirinnee/objex~
var objex = new ObjectX(core);
objex.ExtendPrimitives();
//end~packages.@kirinnee/objex~
//if~target.web~
document.querySelectorAll("button")[0].onclick = function(){
document.getElementById("target").innerText = document.getElementById("input").value.split('').reverse().join('');
};
//end~target.web~

module.exports = {
    Square: Square,
    Rectangle: Rectangle
};
