module.exports = function Rectangle(width, height) {
    this.width = width;
    this.height = height;

    var s = this;
    Object.defineProperties(this, {
        area: {
            get: function () {
                return s.width * s.height;
            }
        }
    });

    this.parameter=  function() {
        return this.width * 2 + this.height * 2;
    }

};
