module.exports = function Square(size) {
    this.size = size;

    var s = this;
    Object.defineProperties(this, {
        area: {
            get: function () {
                return s.size * s.size;
            }
        }
    });

    this.parameter = function() {
        return this.size * 4;
    }

};
