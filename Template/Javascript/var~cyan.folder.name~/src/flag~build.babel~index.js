let Square = require("./Square");
let Rectangle = require("./Rectangle");
let _ = require("lodash"); //flag~packages.lodash~
let Kore = require("@kirinnee/core").Kore; //flag~move.@kirinnee/core~
let ObjectX = require("@kirinnee/objex").ObjectX; //flag~packages.@kirinnee/objex~
require('./flag~build.sass~index.scss');
require("./flag~build.style~index.css");

//if~move.@kirinnee/core~
let core = new Kore();
core.ExtendPrimitives();
//end~move.@kirinnee/core~
//if~packages.@kirinnee/objex~
let objex = new ObjectX(core);
objex.ExtendPrimitives();
//end~packages.@kirinnee/objex~

//if~target.web~
document.querySelectorAll("button")[0].onclick = function(){
document.getElementById("target").innerText = document.getElementById("input").value.split('').reverse().join('');
};
//end~target.web~

module.exports = {
    Square: Square,
    Rectangle: Rectangle
};
