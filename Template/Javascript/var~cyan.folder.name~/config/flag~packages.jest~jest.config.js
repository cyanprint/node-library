module.exports = {
    "roots": [
        "<rootDir>"
    ],
    "testRegex": "(/__tests__/.*|(\\.|/)(test|spec))\\.jsx?$",
    "moduleFileExtensions": [
        "js",
        "jsx",
        "json",
        "node"
    ],
    "moduleNameMapper": { //flag~build.style~
        "\\.(css|scss)$": "identity-obj-proxy" //flag~build.style~
    },//flag~build.style~
    collectCoverageFrom: [
        "src/**/*.js",
        "!src/flag~build.babel~index.js"
    ],

    "coverageThreshold": {
        "global": {
            "branches": 80,
            "functions": 80,
            "lines": 80,
            "statements": 100
        }
    }
};