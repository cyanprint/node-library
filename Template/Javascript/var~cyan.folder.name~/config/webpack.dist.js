const path = require('path');
const optimizer = require("./webpack.optimizer");
const rules = require("./webpack.rules");
const Kore = require("@kirinnee/core").Kore;
const polyfill = require("./flag~polyfill~webpack.polyfill"); //flag~build.distPolyfill~

let core = new Kore();
core.ExtendPrimitives();

let entry = {
    "flag~build.babel~index": "./src/flag~build.babel~index.js",
    "flag!~build.babel~index": "./src/flag!~build.babel~index.js"

};


function GenerateConfig(entry, filename, mode, target, index) {
    if (!index) entry = core.FlattenObject(entry).MapValue(v => core.WrapArray(v)).MapValue(v => polyfill.concat(v)); //flag~build.distPolyfill~flag~polyfill~

    let outDir = path.resolve(__dirname, index ? "../" : "../dist");
    let config = {
        entry: entry,
        output: {
            path: outDir,
            filename: filename,
            libraryTarget: "umd",
            globalObject: "(typeof window !== 'undefined' ? window : this)"
        },
        mode: mode,
        devtool: "source-map", //flag~build.sourceMap~
        module: {rules: rules}
    };
    if (target === "node") {
        config.target = "node";
        config.node = {__dirname: false, __filename: false};
    }
    if (target === "web") config.target = "web";
    if (mode === "production") config.optimization = optimizer;

    return config;
}

let target;
target = "node"; //flag~target.node~
target = "web"; //flag~target.web~

module.exports = [
    GenerateConfig(entry, '[name].min.js', 'production', target, false),
    GenerateConfig(entry, '[name].js', 'development', target, false),
    GenerateConfig(entry, 'index.js', 'production', target, true)
];