let path = require("path"); //flag~build.babel~

//if~build.style~
/*===================
    STYLE LOADER
===================== */
let preCSSLoaders = [
    {loader: 'postcss-loader', options: {config: {path: `${__dirname}/postcss.config.js`}}}, //flag~build.postcss~
    "sass-loader" //flag~build.sass~
];

let use = [
    "style-loader",
    {loader: "css-loader", options: {importLoaders: preCSSLoaders.length}}
].concat(preCSSLoaders);

let cssRule = {
    test: /\.s?css/,
    use: use
};
//end~build.style~

//if~build.babel~
/*===================
    BABEL LOADER
===================== */
let babelLoader = {
    test: /\.m?js$/,
    exclude: /(node_modules|bower_components)/,
    use: {
        loader: 'babel-loader',
        options: {
            configFile: path.resolve(__dirname, "./flag~build.babel~babel.config.js")
        }
    }
};
//end~build.babel~

module.exports = [
    cssRule, //flag~build.style~
    babelLoader, //flag~build.babel~
];
