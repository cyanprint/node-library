module.exports = {
    chunks: {
        "index": "./src/flag!~build.babel~index.js",
        "index": "./src/flag~build.babel~index.js",
    },
    pages: [
        {
            template: "index.html",
            output: "index.html",
            chunks: ['index'],
            title: 'Index'
        },
        {
            template: "index.html",
            output: "home.html",
            chunks: ['index'],
            title: 'Index'
        }
    ],
};