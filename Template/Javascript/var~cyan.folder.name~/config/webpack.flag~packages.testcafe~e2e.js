const path = require('path');
const c = require('./flag~packages.testcafe~pages');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const rules = require("./webpack.rules");
const Kore = require("@kirinnee/core").Kore;
const polyfill = require("./flag~polyfill~webpack.polyfill"); //flag~polyfill~

let core = new Kore();
core.ExtendPrimitives();

let pages = c.pages
    .Map(s => {
        let c = {
            title: s.title || "Index",
            filename: s.output || "index.html",
            chunk: s.chunks || ["index"]
        };
        if (s.template) c.template = path.join("./flag~packages.testcafe~public", s.template);
        return c;
    })
    .Map(s => new HtmlWebpackPlugin(s));

//Add polyfill to each chunk if there is polyfill!
let entry = core.FlattenObject(c.chunks).MapValue(v=>core.WrapArray(v))
    .MapValue(v=>polyfill.concat(v)) //if~polyfill~
    .AsObject();

module.exports = {
    entry: entry,
    output: {
        path: path.resolve(__dirname, "../test/flag~packages.testcafe~e2e/targets/"),
        filename: "[name].js",
        libraryTarget: "umd",
        globalObject: "(typeof window !== 'undefined' ? window : this)"
    },
    plugins: pages,
    mode: "development",
    devtool: "source-map",
    module: {rules: rules},
    target: "web"
};
