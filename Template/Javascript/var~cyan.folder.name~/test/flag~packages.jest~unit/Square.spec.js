let Square = require("../../src/Square");

test("Square", () => {
	let square = new Square(5);
	expect(square.area).toBe(25);
	expect(square.parameter()).toBe(20);
});
